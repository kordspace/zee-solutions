import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";

// core components
import Button from "components/CustomButtons/Button.jsx";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Subject from "@material-ui/icons/Subject";
import glass from "assets/img/glass.jpg";
import iot from "assets/img/iot.jpg";

import sectionsStyle from "assets/jss/material-kit-pro-react/views/presentationSections/sectionsStyle.jsx";

import romania from "assets/img/romania.jpg";


// images array used in rendering a function for this section
import imgs from "assets/img/assets-for-demo/sections/imgs.jsx";

class SectionSections extends React.Component {
  renderContainerFluid(cssClass) {
    return imgs.map(row => {
      return (
        <GridContainer key={row[0]}>
          {row.map((el, index) => {
            return (
              <GridItem
                md={3}
                sm={3}
                xs={12}
                key={`${el}_${index}`}
                className={cssClass}
              >
                <img
                  src={require(`assets/img/assets-for-demo/sections/${el}.jpg`)}
                  alt={el}
                  key={el[index]}
                />
              </GridItem>
            );
          })}
        </GridContainer>
      );
    });
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.sectionSections}>
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem
              md={8}
              className={classNames(classes.mrAuto, classes.mlAuto)}
            >
              <div className={classes.sectionDescription}>
                <h2 className={classes.description}>WHAT WE DO FOR CONSUMERS / SUPPLIERS?</h2>
                <h3 className={classes.description}>
                
                </h3>
                {/*<Link to={"/what-we-do"}>
                  <Button
                    color="rose"
                    target="_blank"
                    className={classes.navButton}
                    round
                  >
                    VIEW OUR SERVICES
                  </Button>
                </Link>*/}
              </div>
            </GridItem>
          </GridContainer>
          <GridContainer>
                  <GridItem xs={12} sm={6} md={6}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${romania})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Consumers
                          </h3>
                          <h3 className={classes.cardTitleWhite}>
                            Purchase certificate and make a difference. 
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={6} md={6}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${glass})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Suppliers
                          </h3>
                          <h3 className={classes.cardTitleWhite}>
                            Upload your recycling to get reimbursed.
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                </GridContainer>
        </div>
        {/*<div className={classes.containerFluid}>
          {this.renderContainerFluid(classes.photoGallery)}
        </div>*/}
      </div>
    );
  }
}

export default withStyles(sectionsStyle)(SectionSections);
