import React from "react";
import { Player } from 'video-react';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";

import "../../../../node_modules/video-react/dist/video-react.css"; // import css
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Share from "@material-ui/icons/Share";
import ChatBubble from "@material-ui/icons/ChatBubble";
import Schedule from "@material-ui/icons/Schedule";
import TrendingUp from "@material-ui/icons/TrendingUp";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";
import People from "@material-ui/icons/People";
import Business from "@material-ui/icons/Business";
import Check from "@material-ui/icons/Check";
import Close from "@material-ui/icons/Close";
import Delete from "@material-ui/icons/Delete";
import Bookmark from "@material-ui/icons/Bookmark";
import Refresh from "@material-ui/icons/Refresh";
import Receipt from "@material-ui/icons/Receipt";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import Info from "components/Typography/Info.jsx";
import Danger from "components/Typography/Danger.jsx";
import Success from "components/Typography/Success.jsx";
import Warning from "components/Typography/Warning.jsx";
import Rose from "components/Typography/Rose.jsx";
import Button from "components/CustomButtons/Button.jsx";

import styles from "assets/jss/material-kit-pro-react/views/homeSections/sectionAbout.jsx";

import cardBlog1 from "assets/img/examples/card-blog1.jpg";
import cardBlog2 from "assets/img/examples/card-blog2.jpg";
import cardBlog3 from "assets/img/examples/card-blog3.jpg";
import cardBlog5 from "assets/img/examples/card-blog5.jpg";
import cardBlog6 from "assets/img/examples/card-blog6.jpg";
import cardProfile1 from "assets/img/examples/card-profile1.jpg";
import cardProfile4 from "assets/img/examples/card-profile4.jpg";
import blog1 from "assets/img/examples/blog1.jpg";
import blog5 from "assets/img/examples/blog5.jpg";
import blog6 from "assets/img/examples/blog6.jpg";
import blog8 from "assets/img/examples/blog8.jpg";
import avatar from "assets/img/faces/avatar.jpg";
import christian from "assets/img/faces/christian.jpg";
import marc from "assets/img/faces/marc.jpg";
import office1 from "assets/img/examples/office1.jpg";
import color1 from "assets/img/examples/color1.jpg";
import color2 from "assets/img/examples/color2.jpg";
import color3 from "assets/img/examples/color3.jpg";
import card1 from "assets/img/about/card1.jpg";
import card2 from "assets/img/about/card2.jpg";
import card3 from "assets/img/about/card3.jpg";
import bottle from "assets/img/about/bottle.jpg";
import infographic from "assets/img/infographic.png";

class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRotate1: "",
            activeRotate2: ""
        };
    }
    componentDidMount() {
        const { classes } = this.props;

        var rotatingCards = document.getElementsByClassName(classes.cardRotate);
        for (let i = 0; i < rotatingCards.length; i++) {
            var rotatingCard = rotatingCards[i];
            var rotatingWrapper = rotatingCard.parentElement;
            var cardWidth = rotatingCard.parentElement.offsetWidth;
            var cardHeight = rotatingCard.children[0].children[0].offsetHeight;
            rotatingWrapper.style.height = cardHeight + "px";
            rotatingWrapper.style["margin-bottom"] = 30 + "px";
            var cardFront = rotatingCard.getElementsByClassName(classes.front)[0];
            var cardBack = rotatingCard.getElementsByClassName(classes.back)[0];
            cardFront.style.height = cardHeight + 35 + "px";
            cardFront.style.width = cardWidth + "px";
            cardBack.style.height = cardHeight + 35 + "px";
            cardBack.style.width = cardWidth + "px";
        }
    }
    render() {
        const { classes, ...rest } = this.props;
        const { about } = this.props;

        if (about) {
            return (
                <div {...rest} className="cd-section" className={classes.aboutMain}>
                    <div className={classes.sectionGray}>
                        <div>
                            {/* DYNAMIC COLORED SHADOWS START */}
                            <div className={classes.container}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <div className={classes.titleClear} id="about">
                                            <div className={classes.title}>
                                                <h2 className="center">{about[0].content}</h2>
                                                <h4 className="center">{about[1].content}</h4>
                                                <h3 className="center">{about[2].content}
                                                </h3>
                                                <br />
                                            </div>
                                        </div>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer className={classes.aboutContainer}>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Card blog className={classes.aboutCard}>
                                            <CardHeader image>
                                                <img src={about[3].imagelink} alt="a group of people fistbumping" />
                                                <div
                                                    className={classes.coloredShadow}
                                                    style={{
                                                        backgroundImage: `url(${about[3].imagelink})`,
                                                        opacity: "1"
                                                    }}
                                                />
                                            </CardHeader>
                                            <CardBody className={classes.aboutCardBody}>
                                                {/*<Warning>
                        <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                        </Warning>*/}
                                                <h3 className={classes.aboutCardDescription}>
                                                    {about[3].content}
                                                </h3>
                                                <h5 className={classes.aboutCardDescription}>
                                                    {about[4].content}
                                                    <br></br><br></br><br></br> </h5>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Card blog className={classes.aboutCard}>
                                            <CardHeader image>
                                                <img src={about[5].imagelink} alt="a glass and a glass bottle" />
                                                <div
                                                    className={classes.coloredShadow}
                                                    style={{
                                                        backgroundImage: `url(${about[5].imagelink})`,
                                                        opacity: "1"
                                                    }}
                                                />
                                            </CardHeader>
                                            <CardBody className={classes.aboutCardBody}>
                                                {/*<Warning>
                        <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                        </Warning>*/}
                                                <h3 className={classes.aboutCardDescription}>
                                                    {about[5].content}
                                                </h3>
                                                <h5 className={classes.aboutCardDescription}>
                                                    {about[6].content}
                                                </h5>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Card blog className={classes.aboutCard}>
                                            <CardHeader image>
                                                <img src={about[7].imagelink} alt="network of nodes over the globe" />
                                                <div
                                                    className={classes.coloredShadow}
                                                    style={{
                                                        backgroundImage: `url(${about[7].imagelink})`,
                                                        opacity: "1"
                                                    }}
                                                />
                                            </CardHeader>
                                            <CardBody className={classes.aboutCardBody}>
                                                {/*<Warning>
                        <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                        </Warning>*/}
                                                <h3 className={classes.aboutCardDescription}>
                                                    {about[7].content}
                                                </h3>
                                                <h5 className={classes.aboutCardDescription}>
                                                    {about[8].content}
                                                </h5>
                                            </CardBody>
                                        </Card>
                                    </GridItem>
                                </GridContainer>
                                <Player
                                    playsInline
                                    poster={about[9].content}
                                    src={about[10].content}
                                /><br></br><br></br>
                            </div>
                            {/* DYNAMIC COLORED SHADOWS END */}
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            )
        }
    }
}

export default withStyles(styles)(About);
