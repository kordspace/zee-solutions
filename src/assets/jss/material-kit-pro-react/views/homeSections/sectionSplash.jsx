import {
    container,
    coloredShadow,
    title,
    cardTitle,
    description,
    mlAuto,
    infoColor,
    roseColor
  } from "assets/jss/material-kit-pro-react.jsx";
  
  import imageStyles from "assets/jss/material-kit-pro-react/imagesStyles.jsx";
  
  import rotatingCards from "assets/jss/material-kit-pro-react/rotatingCards.jsx";
  
  const styles = {
    container,
    coloredShadow,
    title,
    mlAuto,
    cardTitle,
    ...imageStyles,
    ...rotatingCards,
    aboutCard: {
      marginTop: "-20px"
    },
    aboutMain: {
      marginTop: "50px"
    },
    splash: {
        height: "100vh",
        textAlign: "center",
        color: "white",
        background: "#bace3f",
        background: "-moz-linear-gradient(-45deg, #bace3f 0%, #39a585 50%, #3a7ab7 100%)",
        background: "-webkit-linear-gradient(-45deg, #bace3f 0%,#39a585 50%,#3a7ab7 100%)",
        background: "linear-gradient(135deg, #bace3f 0%,#39a585 50%,#3a7ab7 100%)",
        filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#bace3f', endColorstr='#3a7ab7',GradientType=1 )", 
    },
    splashContainer: {
        height: "50vh",
        width: "20%",
        paddingTop: "30vh",
        textAlign: "center",
        color: "white",
        margin: "0 auto",
    },
    splashImage: {
        height: "50vh",
        backgroundImage: "url(https://firebasestorage.googleapis.com/v0/b/end-of-waste.appspot.com/o/splash.png?alt=media&token=eb2b44a8-d65a-4eb4-ae07-91fc6ea26d10)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover"
    },
    container: {
      marginBottom: "-50px",
      marginLeft: "4rem",
      marginRight: "4rem",
      '@media (max-width: 525px)': {
        marginLeft: "1rem",
        marginRight: "1rem",
        }
    },
    titleClear: {
      marginTop: "-100px",
    },
    title: {
      marginLeft: "1rem",
      marginRight: "1rem",
      "& h2": {
        paddingTop: "150px",
        marginBottom: "20px",
        color: "#3A7AB7",
        fontSize: "3rem",
        lineHeight: "3rem",
        '@media (max-width: 525px)': {
          fontSize: "2em",
          lineHeight: "2rem",
        }
      },
      "& h4": {
        marginBottom: "40px",
        color: "#BACE3F",
        fontSize: "2.2rem",
        lineHeight: "2.5rem",
        '@media (max-width: 525px)': {
          fontSize: "1.5em",
          lineHeight: "1.5rem",
        }
      },
      "& h3": {
        marginTop: "-0.5rem",
        color: "#000",
        fontFamily: "Noto Serif",
        fontSize: "1.3rem",
        lineHeight: "2rem",
        zIndex: "0",
        '@media (max-width: 525px)': {
          fontSize: "1rem",
          marginTop: "0rem",
          lineHeight: "1.5rem",
        }
      }
    },
    aboutInfographicContainer: {
    },
    aboutInfographic: {
    },
    aboutCard: {
      backgroundColor: "#fafafa"
    },
    aboutCardInfographic: {
      backgroundColor: "#F7F7F7",
      margin: "0 auto",
      width: "80%",
      marginTop: "2rem",
      marginBottom: "4rem",
      '@media (max-width: 959px)': {
        width: "100%",
      }
    },
    aboutCardBody: {
      "& h5": {
        fontFamily: "Noto Serif",
      },
      "& h3": {
        color: "#3A7AB7 !important",
      }
    },
    aboutContainer: {
      marginTop: "-20px"
    },
    aboutCardDescription: {
      marginTop: "-20px"
    },
    sectionGray: {
    },
    sectionWhite: {
      background: "#FFFFFF"
    },
    cardAbout: {
      marginTop: "20px"
    },
    cardTitleAbsolute: {
      ...cardTitle,
      position: "absolute !important",
      bottom: "15px !important",
      left: "15px !important",
      color: "#fff !important",
      fontSize: "1.125rem !important",
      textShadow: "0 2px 5px rgba(33, 33, 33, 0.5) !important"
    },
    cardTitleWhite: {
      "&, & a": {
        ...title,
        marginTop: ".625rem",
        marginBottom: "0",
        minHeight: "auto",
        color: "#fff !important"
      }
    },
    cardCategory: {
      marginTop: "10px",
      "& svg": {
        position: "relative",
        top: "8px"
      }
    },
    cardCategorySocial: {
      marginTop: "10px",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "22px",
        position: "relative",
        marginTop: "-4px",
        top: "2px",
        marginRight: "5px"
      },
      "& svg": {
        position: "relative",
        top: "5px"
      }
    },
    cardCategorySocialWhite: {
      marginTop: "10px",
      color: "rgba(255, 255, 255, 0.8)",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "22px",
        position: "relative",
        marginTop: "-4px",
        top: "2px",
        marginRight: "5px"
      },
      "& svg": {
        position: "relative",
        top: "5px"
      }
    },
    cardCategoryWhite: {
      marginTop: "10px",
      color: "rgba(255, 255, 255, 0.7)"
    },
    cardCategoryFullWhite: {
      marginTop: "10px",
      color: "#FFFFFF"
    },
    cardDescription: {
      ...description
    },
    cardDescriptionWhite: {
      color: "rgba(255, 255, 255, 0.8)"
    },
    aboutCardDescription: {
      color: "#000000 !important",
      textAlign: "center"
    },
    author: {
      display: "inline-flex",
      "& a": {
        color: "#3C4858"
      }
    },
    authorWhite: {
      display: "inline-flex",
      "& a": {
        color: "rgba(255, 255, 255, 0.8)"
      }
    },
    avatar: {
      width: "30px",
      height: "30px",
      overflow: "hidden",
      borderRadius: "50%",
      marginRight: "5px"
    },
    statsWhite: {
      color: "rgba(255, 255, 255, 0.8)",
      display: "inline-flex",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        fontSize: "18px",
        lineHeight: "18px"
      },
      "& svg": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        width: "18px",
        height: "18px"
      }
    },
    stats: {
      color: "#999999",
      display: "flex",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        fontSize: "18px",
        lineHeight: "18px"
      },
      "& svg": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        width: "18px",
        height: "18px"
      }
    },
    justifyContentCenter: {
      WebkitBoxPack: "center !important",
      MsFlexPack: "center !important",
      justifyContent: "center !important"
    },
    iconWrapper: {
      color: "rgba(255, 255, 255, 0.76)",
      margin: "10px auto 0",
      width: "130px",
      height: "130px",
      border: "1px solid #E5E5E5",
      borderRadius: "50%",
      lineHeight: "174px",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "55px",
        lineHeight: "55px"
      },
      "& svg": {
        width: "55px",
        height: "55px"
      }
    },
    iconWrapperColor: {
      borderColor: "rgba(255, 255, 255, 0.25)"
    },
    iconWhite: {
      color: "#FFFFFF"
    },
    iconRose: {
      color: roseColor
    },
    iconInfo: {
      color: infoColor
    },
    marginTop30: {
      marginTop: "30px"
    },
    textCenter: {
      textAlign: "center"
    },
    marginBottom20: {
      marginBottom: "20px"
    }
  };
  
  export default styles;
  