import {
    container,
    main,
    mainRaised,
    title,
    section
  } from "assets/jss/material-kit-pro-react.jsx";
  
  const componentsStyle = {
    main,
    mainRaised,
    parallax: {
      minHeight: "100vh",
      overflow: "hidden",
    },
    socialFooterNav: {
      marginTop: "3rem"
    },
    mainRaised: {
      marginLeft: "40px",
      marginRight: "40px",
      borderRadius: "0.5rem",
      boxShadow: "0px 0px 25px -7px black"
    },
    navButton: {
      backgroundColor: "rgba(57, 165, 133, 1)",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      width: "50%",
      zIndex: "1"
    },
    container: {
      ...container,
      zIndex: "2",
      position: "relative"
    },
    carouselHero: {
      height: "100vh",
      minHeight: "600px",
      zIndex: "999"
    },
    carouselContent: {
      "& img": {
        height: "100vh",
        minHeight: "600px",
      },
      "& h1": {
        marginTop: "-70vh",
        color: "white",
        textAlign: "center",
        '@media (max-width: 499px)': {
          fontSize: "2rem",
        },    
        '@media (min-width: 500px)': {
          fontSize: "3rem",
        },    
        '@media (min-width: 1000px)': {
          fontSize: "4rem",
        },    
        '@media (min-width: 2000px)': {
          fontSize: "7rem",
        },    
      },
      "& h2": {
        color: "white",
        textAlign: "center",        
        '@media (max-width: 499px)': {
          fontSize: "1rem",
        },    
        '@media (min-width: 500px)': {
          fontSize: "2rem",
        },    
        '@media (min-width: 1000px)': {
          fontSize: "3rem",
        },    
        '@media (min-width: 2000px)': {
          fontSize: "5rem",
        },    

      }
    },
    carouselNavButton: {
      backgroundColor: "rgba(57, 165, 133, 1)",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      marginTop: "2rem",
      zIndex: "1",
      '@media (max-width: 499px)': {
        fontSize: ".5rem",
      },    
      '@media (min-width: 500px)': {
        fontSize: "1rem",
      },    
      '@media (min-width: 1000px)': {
        fontSize: "1.5rem",
      },    
      '@media (min-width: 2000px)': {
        fontSize: "3rem",
      },    
    },  
    about: {
    },
    brand: {
      color: "#FFFFFF",
      textAlign: "center",
      "& h1": {
        fontSize: "3rem",
        fontWeight: "700",
        fontFamily: "Lato",       
        display: "inline-block",
        position: "relative",
        textShadow: "0px 0px 10px rgba(0,0,0, 0.5)",
        '@media (min-width: 992px)': {
          fontSize: "3.5em"
        },
        '@media (max-width: 525px)': {
          fontSize: "2.3em"
        }
      },
      "& h2": {
        fontSize: "1.8rem",
        marginBottom: "20px",
        textShadow: "0px 0px 10px rgba(0,0,0, 0.5)",
        '@media (min-width: 992px)': {
          fontSize: "3em"
        },
        '@media (max-width: 525px)': {
          fontSize: "1.5em",
          marginBottom: "0px",
        }
      },
      "& h3": {
        fontSize: "1.8rem",
        fontFamily: "Noto Serif",
        margin: "10px auto 0",
        textShadow: "0px 0px 10px rgba(0,0,0, 0.5)",
        '@media (max-width: 525px)': {
          fontSize: "1.2em"
        }
      },
    },
    footerContainer: {
      marginTop: "1rem"
    },
    footerButton: {
      marginTop: "-1rem"
    },
    title: {
      ...title,
      color: "#FFFFFF !important"
    },
    link: {
      textDecoration: "none"
    },
    textCenter: {
      textAlign: "center"
    },
    proBadge: {
      position: "absolute",
      fontSize: "22px",
      textTransform: "uppercase",
      fontWeight: "bold",
      right: "-90px",
      top: "-3px",
      padding: "10px 18px",
      backgroundColor: "#fff",
      borderRadius: "3px",
      color: "#444",
      lineHeight: "22px",
      boxShadow: "0px 5px 5px -2px rgba(31,31,31,0.4)"
    },
    section: {
      ...section,
      padding: "70px 0px"
    },
    sectionGray: {
      background: "#e5e5e5"
    },
    block: {
      color: "white",
      padding: "0.9375rem",
      fontWeight: "500",
      fontSize: "12px",
      textTransform: "uppercase",
      borderRadius: "3px",
      textDecoration: "none",
      position: "relative",
      display: "block"
    },
    inlineBlock: {
      display: "inline-block",
      padding: "0px",
      width: "auto"
    },
    list: {
      marginBottom: "0",
      padding: "0",
      marginTop: "0"
    },
    left: {
      float: "left!important",
      display: "block"
    },
    right: {
      padding: "15px 0",
      margin: "0",
      float: "right"
    },
    icon: {
      width: "18px",
      height: "18px",
      top: "3px",
      position: "relative"
    }
  };
  
  export default componentsStyle;
  