import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDLHY4OQtZ2H_1L29AppjfedM_w7fPTnWs",
  authDomain: "aushi-mizani.firebaseapp.com",
  databaseURL: "https://aushi-mizani.firebaseio.com",
  projectId: "aushi-mizani",
  storageBucket: "aushi-mizani.appspot.com",
  messagingSenderId: "167040192570"
};
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true }); 

  export default firebase;