import React from 'react'

const PaypalSubscriptionButton = () => (
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" defaultValue="_s-xclick" />
    <input type="hidden" name="hosted_button_id" defaultValue="6P8AHEBL8QU9C" />
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border={0} name="submit" alt="PayPal - The safer, easier way to pay online!" />
    <img alt border={0} src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width={1} height={1} />
  </form>
)

export default PaypalSubscriptionButton

